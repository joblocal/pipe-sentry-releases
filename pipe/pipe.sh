#!/usr/bin/env bash
#
# This pipe can be used for creating a sentry release and deployment for a single sentry project
#

source "$(dirname "$0")/common.sh"

# Required parameters
export SENTRY_TOKEN=${SENTRY_TOKEN:?'SENTRY_TOKEN variable missing.'}
export SENTRY_ORGANIZATION_SLUG=${SENTRY_ORGANIZATION_SLUG:?'SENTRY_ORGANIZATION_SLUG variable missing.'}

# Default parameters
export SENTRY_PROJECT_NAME=${SENTRY_PROJECT_NAME:=${BITBUCKET_REPO_SLUG}}
export SENTRY_VERSION_IDENTIFIER=${SENTRY_VERSION_IDENTIFIER:=${BITBUCKET_COMMIT}}
export SENTRY_ENVIRONMENT_NAME=${SENTRY_ENVIRONMENT_NAME:='production'}
export SENTRY_URL=${SENTRY_URL:='sentry.io'}

info "Creating Sentry release"

run curl -k https://${SENTRY_URL}/api/0/organizations/${SENTRY_ORGANIZATION_SLUG}/releases/ \
    -f \
    -X POST \
    -H "Authorization: Bearer ${SENTRY_TOKEN}" \
    -H "Content-Type:application/json" \
    -d "{
          \"version\":\"${SENTRY_VERSION_IDENTIFIER}\",
          \"refs\":[
            {
              \"repository\":\"${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}\",
              \"commit\":\"${BITBUCKET_COMMIT}\"
            }
          ],
          \"projects\": [
            \"${SENTRY_PROJECT_NAME}\"
          ]
        }"

if [[ "${status}" == "0" ]]; then
  success "Successfully created Sentry release with version '${SENTRY_VERSION_IDENTIFIER}'"
else
  fail "Could not create new Sentry release!"
fi

info "Creating Sentry deployment"

run curl -k https://${SENTRY_URL}/api/0/organizations/joblocal-gmbh/releases/${SENTRY_VERSION_IDENTIFIER}/deploys/ \
    -f \
    -H "Authorization: Bearer ${SENTRY_TOKEN}" \
    -X POST \
    -H "Content-Type:application/json" \
    -d "{
          \"environment\":\"${SENTRY_ENVIRONMENT_NAME}\",
          \"name\":\"${BITBUCKET_BUILD_NUMBER}\"
        }"

if [[ "${status}" == "0" ]]; then
  success "Successfully created Sentry deployment to ${SENTRY_ENVIRONMENT_NAME} for version '${SENTRY_VERSION_IDENTIFIER}'"
else
  fail "Could not create Sentry deployment!"
fi
