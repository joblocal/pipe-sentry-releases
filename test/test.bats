#!/usr/bin/env bats

setup() {
  DOCKER_IMAGE="test/pipe-sentry-releases"
}

@test "Check if SENTRY_TOKEN is required" {
    run docker run \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}:test

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 1 ]
    [[ "$output" =~ "SENTRY_TOKEN: SENTRY_TOKEN variable missing." ]]
}

@test "Check if SENTRY_ORGANIZATION_SLUG is required" {
    run docker run \
        -e SENTRY_TOKEN="sentrytoken" \
        -v $(pwd):$(pwd) \
        -w $(pwd) \
        ${DOCKER_IMAGE}:test

    echo "Status: $status"
    echo "Output: $output"

    [ "$status" -eq 1 ]
    [[ "$output" =~ "SENTRY_ORGANIZATION_SLUG: SENTRY_ORGANIZATION_SLUG variable missing." ]]
}
