# pipe-sentry-releases
This pipe can be used for creating a new release for a project on [Sentry](https://sentry.io) from Bitbucket Pipelines.

## Usage

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
script:
  - pipe: joblocal/pipe-sentry-releases:latest
    variables:
      SENTRY_TOKEN: "<string>"
      SENTRY_ORGANIZATION_SLUG: "<string>"
      # SENTRY_PROJECT_NAME: "<string>" # Optional
      # SENTRY_ENVIRONMENT_NAME: "<string>" # Optional
      # SENTRY_VERSION_IDENTIFIER: "<string>" # Optional
```
## Variables

| Variable                        | Usage                                                       |
| ------------------------------- | ----------------------------------------------------------- |
| SENTRY_TOKEN (*)                | The Sentry Authentication Token for communicating with the API |
| SENTRY_ORGANIZATION_SLUG (*)    | The slug of your Sentry organization |
| SENTRY_PROJECT_NAME             | The name of the sentry project you want to create a release for. Default: `BITBUCKET_REPO_SLUG`. |
| SENTRY_ENVIRONMENT_NAME         | The name of the environment on sentry create a deployment for. Default: `production`. |
| SENTRY_VERSION_IDENTIFIER       | The identifier of the new release. Default: `BITBUCKET_COMMIT`. |

_(*) = required variable._

## Prerequisites
Sentry needs to be configured and you need your Api token to be able to use this pipe.

## Examples

Basic example:

```yaml
script:
  - pipe: joblocal/pipe-sentry-releases:latest
    variables:
      SENTRY_TOKEN: "foobar"
      SENTRY_ORGANIZATION_SLUG: "barfoo"
```

Advanced example:

```yaml
script:
  - pipe: joblocal/pipe-sentry-releases:latest
    variables:
      SENTRY_TOKEN: "foobar"
      SENTRY_ORGANIZATION_SLUG: "barfoo"
      SENTRY_PROJECT_NAME: "my-project"
      SENTRY_ENVIRONMENT_NAME: "staging"
      SENTRY_VERSION_IDENTIFIER: $BITBUCKET_BUILD_NUMBER
```

## Contributing
Please read through our [contributing guidelines](https://bitbucket.org/joblocal/pipe-sentry-releases/src/master/CONTRIBUTING.md). Included are directions for opening issues, coding standards, and feature requests.

## Tests
To execute the tests, you'll need [Composer](https://getcomposer.org/), [Docker](https://www.docker.com/) and [bats](https://github.com/sstephenson/bats).
Once those are set up, you can start the tests by running ```composer run test```

## Authors
* **Joblocal GmbH** - *Initial work* - [Joblocal](https://github.com/joblocal)
